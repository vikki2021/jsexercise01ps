import { bds } from './src/data';
import './styles.scss';

console.log(bds);

// working on showing the first 30 words..

//

const app = document.getElementById('app');
// app.innerHTML += '<img src="images/kitten.jpg" style="width:100px;" />';

function render() {
  let docName = "<div class='docName'><h1>Our collection of books</h1></div>";

  for (const bd of bds) {
    const book = `<div class="container" id="${bd.id}">`;
    const title = `<h3 class='title'>${bd.titre}</h3>`;
    const id = `<p>Book ID:${bd.id}</p>`;
    const editor = `<p>Editor:${bd.editeur}</p>`;
    const collection = `<p>collection:${bd.collection}</p>`;
    const series = `<p>series:${bd.serie}</p>`;
    const description = `<p class='description'>description:${bd.resume.substring(0, 30)}</p>`;
    const type = `<p>type:${bd.type}</p>`;
    const state = `<p>state:${bd.etat}</p>`;
    const image = `<p><img src="images/${bd.image}" style="width:100px;" alt="Image from ${bd.image} not found. "></p>`;
    const isbn = `<p>isbn:${bd.isbn}</p>`;
    const year = `<p>year:${bd.annee_de_parution}</p>`;
    const notome = `<p>notome:${bd.no_tome}</p>`;
    const price = `<p>price:${bd.prix}</p>`;
    const themes = `<p>themes:${bd.themes}</p>`;
    const authors = `<p>authors:${bd.auteurs}</p>`;
    const like = `<p>like:${bd.like}</
  p>`;
    const rent = `<p>rent:${bd.emprunt}</p>`;
    const bookEnd = '</div>';
    const btnReadMore = '<span class="toto">...<button type="button" class="btn-more">Read More</button></span>';
    docName += `${book + title + image + id + editor + collection + series + type + state + isbn + year + notome + price + themes + authors + description + btnReadMore + like + rent + bookEnd}<br>`;
  }

  app.innerHTML = docName;

  // Event for clicking the btn 'read more'/read less

  document.body.addEventListener('click', (e) => {
    if (e.target.matches('.btn-more')) {
    // console.log(e.target.parentNode.parentNode.id);
      const { id } = e.target.parentNode.parentNode;// why?
      // console.log(id);
      const content = document.getElementById(id);// all content
      // console.log(content);
      const description = content.querySelector('.description');// description
      // console.log(description);
      description.innerHTML = `description:<span>${bds[id].resume}<button type='button' class='btn-less'>Read Less</button></span>`;
    }

    if (e.target.matches('.btn-less')) {
      console.log(e.target.parentNode.parentNode.id);
      const { id } = e.target.parentNode.parentNode;
      console.log(id);
      const content = document.getElementById(id);
      const description = content.querySelector('.description');
      description.innerHTML = `
  description: ${bds[id].resume.substring(0, 30)}
    <span>...<button type='button' class='btn-more'>Read More</button></span>
    `;
    }
  });
}

render();
// import $ from 'jquery';
// import 'bootstrap';
// $('body').append('jquery + bootstrap works!');
// const app = document.getElementById('app');

// // every files in "static" folder can be used directly like that
// app.innerHTML += '<img src="images/kitten.jpg" style="width:100px;" />';
